import {http} from './config.js'

export default {
	list: (page,search=null) => {
		return http.get('/rest/tempo/previsao-numerica?page=' + page + (search==null || search == '' ? '' : search))
	}
}