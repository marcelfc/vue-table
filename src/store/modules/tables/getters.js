export default {
	listItens: state => {
		return state.itens
	},
	getNumberOfPages: state => {
		return state.numberOfPages
	}
}