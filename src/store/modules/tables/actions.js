export default {
	alterItens(context, value) {
		context.commit('ALTER_ITENS', value)
	},
	alterNumberOfPages(context, value) {
		context.commit('ALTER_NUMBER_OF_PAGES', value)
	}
}